import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const ImageDetail = prop =>{
console.log(prop);
    return (
    <View>
    <Image source={prop.imageSource} />
    <Text>{prop.title}</Text>
    <Text>Image Score - {prop.score}</Text>
    </View>
    );
};

const styles=StyleSheet.create({});

export default ImageDetail;