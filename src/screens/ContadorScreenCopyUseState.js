import React,{useState} from "react";
import { Text, StyleSheet, View, Button} from "react-native";

const ContadorScreen=()=>{
const [counter, setCounter]=useState(0);


return(
<View>
    <Button
        title="Incrementar"
        onPress={()=>{
           setCounter(counter+1);
        }}
    />
    <Button
        title="Decremento"
        onPress={()=>{
            setCounter(counter-1);
        }}
    />
    <Text>Contador: {counter}</Text>
</View>
);
};

const styles=StyleSheet.create({});

export default ContadorScreen;