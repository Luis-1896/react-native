import React from 'react';
import { Text, StyleSheet, View,Button } from 'react-native';
const ComponentsScreen = ({navigation}) => {
/*
const greeting='Pumba';
const text=<Text>Lana</Text>*/
  /*  return (
    <View>
        <Text style = { styles.textStyle } > This is the components screen </Text>
     <Text>{greeting}</Text>
     {text}
     </View>
    );*/
    /*
    return <View>
    <Text style = { styles.textStyle } > This is the components screen < /Text>
    <Text>{greeting}</Text>
    {text}
    </View>;*/
const name='Luis';
const suma=(a,b)=>{
  return a+b;
};

return(
<View>
<Text style={[styles.textStyle,styles.text1]}>Getting started with React Native</Text>
<Text style={styles.subHeaderStyle}> My name is {name}</Text>
    <Text style={styles.subHeaderStyle}> {suma(1,3)}</Text>
    <Button
    title={'regresar'}
    onPress={()=>navigation.navigate('Home')}
    />

</View>
);


};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 45
    },
    subHeaderStyle:{
    fontSize:20
    },
    text1:{
        color:'red'
    }
});

export default ComponentsScreen;